package com.ofbusiness.assignment.repository;

import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ofbusiness.assignment.domain.Chatlog;


@Repository
public interface ChatlogRepository extends JpaRepository<Chatlog, Long> {

	@Transactional
	@Modifying
	@Query(value = "UPDATE chatlog set is_deleted = 1 , is_deleted_on = GETDATE() where user_id =:userId AND chat_log_id =:chatId", nativeQuery = true)
	public int deleteUserChatByMsgId(String userId,long chatId) throws Exception;

	@Transactional
	@Modifying
	@Query(value = "UPDATE chatlog set is_deleted = 1 , is_deleted_on = GETDATE() where user_id =:userId", nativeQuery = true)
	public int deleteUserChatLog(String userId) throws Exception;
	
	List<Chatlog> findAll(Specification<Chatlog> specification, Pageable pageElements);
	
	@Query(value = "SELECT COUNT(user_id) from chatlog where user_id =:userId", nativeQuery = true)
	public int checkUserExist(String userId) throws Exception;

	@Query(value = "SELECT COUNT(chat_log_id) from chatlog where chat_log_id =:chatId AND is_deleted = 0", nativeQuery = true)
	public int checkChatIdExist(int chatId) throws Exception;
}
