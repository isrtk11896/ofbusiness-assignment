package com.ofbusiness.assignment.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Chatlog")
public class Chatlog {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "chatLog_id", nullable = false)
	private long chatLogId = 0;

	@Column(name = "message_text", nullable = false)
	private String messageText;

	@Column(name = "user_id", nullable = false)
	private String userId;

	@Column(name = "timestamp", nullable = false)
	private Date timestamp;

	@Column(name = "is_sent", nullable = false)
	private int isSent;

	@Column(name = "is_deleted", nullable = false)
	private boolean isDeleted = false;

	@Column(name = "is_deleted_on", nullable = true)
	private Date isDeletedOn;

	public long getChatLogId() {
		return chatLogId;
	}

	public void setChatLogId(long chatLogId) {
		this.chatLogId = chatLogId;
	}

	public String getMessageText() {
		return messageText;
	}

	public void setMessageText(String messageText) {
		this.messageText = messageText;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public int getIsSent() {
		return isSent;
	}

	public void setIsSent(int isSent) {
		this.isSent = isSent;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Date getIsDeletedOn() {
		return isDeletedOn;
	}

	public void setIsDeletedOn(Date isDeletedOn) {
		this.isDeletedOn = isDeletedOn;
	}

}
