package com.ofbusiness.assignment.constants;

public interface APIEndPoints {
	
	public static final String BASEURL = "/api";
	
	public static final String CHATLOG = "/chatlogs";

}
