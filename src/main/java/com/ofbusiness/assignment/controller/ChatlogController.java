package com.ofbusiness.assignment.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ofbusiness.assignment.constants.APIEndPoints;
import com.ofbusiness.assignment.domain.Chatlog;
import com.ofbusiness.assignment.dto.ChatlogDto;
import com.ofbusiness.assignment.response.APIResponse;
import com.ofbusiness.assignment.service.ChatlogService;

@RestController
@RequestMapping(APIEndPoints.BASEURL + APIEndPoints.CHATLOG)
public class ChatlogController {

	private static final Logger log = LoggerFactory.getLogger(ChatlogController.class);

	@Autowired
	private ChatlogService chatLogService;

	@PostMapping(value = "/{userId}", produces = "application/json;charset=UTF-8")
	public ResponseEntity<APIResponse> createChatLogEntry(@PathVariable("userId") String userId,
			@RequestBody ChatlogDto chatLogDto, HttpServletRequest request) {

		log.info("{}: Recieved request to create chatlog entry from client address :{}", request.getSession().getId(),
				request.getLocalAddr());

		log.debug("{}: Recieved [userId] as paramter- {}", request.getSession().getId(), userId);

		try {
			long chatLogId = chatLogService.createChatLogEntry(chatLogDto, userId);

			log.info("{} : Chatlog entry has been created successfully [ChatLogId]: {}", request.getSession().getId(),
					chatLogId);

			return new ResponseEntity<APIResponse>(
					new APIResponse(chatLogId, "Chatlog entey has been created successfully", true), HttpStatus.OK);
		} catch (IllegalArgumentException e) {

			log.error("{} : Got error while creating chatlog entry : {}", request.getSession().getId(), e.getMessage());

			return new ResponseEntity<APIResponse>(new APIResponse(null, e.getMessage(), false),
					HttpStatus.BAD_REQUEST);
		} catch (Exception e) {

			log.error("{} : Got error while creating chatlog entry : {}", request.getSession().getId(), e.getMessage());

			return new ResponseEntity<APIResponse>(new APIResponse(null, e.getMessage(), false),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@GetMapping(value = "/{userId}", produces = "application/json;charset=UTF-8")
	public ResponseEntity<APIResponse> getUserChatLogs(HttpServletRequest request,
			@PathVariable(name = "userId") String userId,
			@RequestParam(name = "limit", required = false, defaultValue = "10") String limit,
			@RequestParam(name = "page", required = false, defaultValue = "0") String page) {

		log.info("{} : Recieved request to Get user chat logs", request.getSession().getId());

		log.debug("{}: Recieved [userId] as paramter: {}", request.getSession().getId(), userId);

		List<Chatlog> chatLogs = new ArrayList<>();
		try {
			chatLogs = chatLogService.retrieveChatLogByUserId(userId, Integer.valueOf(limit), Integer.valueOf(page));
			log.info("{} : Retrieved result for chat logs for userId [" + userId + "]: {}",
					request.getSession().getId(), chatLogs.toArray());

			return new ResponseEntity<APIResponse>(
					new APIResponse(chatLogs, "Chatlog has been retrieved successfully", true), HttpStatus.OK);
		} catch (IllegalArgumentException e) {

			log.error("{} : Got error while retrieving chatlog entry : {}", request.getSession().getId(),
					e.getMessage());

			return new ResponseEntity<APIResponse>(new APIResponse(null, e.getMessage(), false),
					HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			log.error("{} : Unexpected Exception occured : {}", request.getSession().getId(), e.getMessage());
			return new ResponseEntity<APIResponse>(new APIResponse(null, e.getMessage(), false),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@DeleteMapping(value = "/{userId}/{chatId}", produces = "application/json;charset=UTF-8")
	public ResponseEntity<APIResponse> deleteUserMessageByChatId(@PathVariable("userId") String userId,
			@PathVariable("chatId") String chatId, HttpServletRequest request) {

		log.info("{}: Recieved request to delete user chatlog by chat Id from client address :{}",
				request.getSession().getId(), request.getLocalAddr());

		log.debug("{}: Recieved [userId] as paramter :{}", request.getSession().getId(), userId);

		log.debug("{}: Recieved [chatId] as paramter :{}", request.getSession().getId(), chatId);

		try {

			chatLogService.removeUserChatLogByMsgId(userId, Integer.parseInt(chatId));
			log.info("{} : Chatlog entry has been deleted successfully", request.getSession().getId());
			return new ResponseEntity<APIResponse>(
					new APIResponse(null, "Chatlog entry has been deleted successfully", true), HttpStatus.OK);

		} catch (IllegalArgumentException e) {

			log.error("{} : Got error while deleting chatlog entry : {}", request.getSession().getId(), e.getMessage());

			return new ResponseEntity<APIResponse>(new APIResponse(null, e.getMessage(), false),
					HttpStatus.BAD_REQUEST);
		} catch (Exception e) {

			log.error("{} : Got error while deleting chatlog entry : {}", request.getSession().getId(), e.getMessage());

			return new ResponseEntity<APIResponse>(new APIResponse(null, e.getMessage(), false),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@DeleteMapping(value = "/{userId}", produces = "application/json;charset=UTF-8")
	public ResponseEntity<APIResponse> deleteUserChatLog(@PathVariable("userId") String userId,
			HttpServletRequest request) {

		log.info("{}: Recieved request to delete user chatlog from client address :{}", request.getSession().getId(),
				request.getLocalAddr());

		log.debug("{}: Recieved [userId] as paramter :{}", request.getSession().getId(), userId);

		try {

			chatLogService.removeUserChatLog(userId);
			log.info("{} : Chatlog entry has been deleted successfully", request.getSession().getId());
			return new ResponseEntity<APIResponse>(
					new APIResponse(null, "Chatlog entry has been deleted successfully", true), HttpStatus.OK);

		} catch (IllegalArgumentException e) {

			log.error("{} : Got error while deleting chatlog entry : {}", request.getSession().getId(), e.getMessage());

			return new ResponseEntity<APIResponse>(new APIResponse(null, e.getMessage(), false),
					HttpStatus.BAD_REQUEST);
		} catch (Exception e) {

			log.error("{} : Got error while deleting chatlog entry : {}", request.getSession().getId(), e.getMessage());

			return new ResponseEntity<APIResponse>(new APIResponse(null, e.getMessage(), false),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}
}