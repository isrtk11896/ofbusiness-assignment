package com.ofbusiness.assignment.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.ofbusiness.assignment.domain.Chatlog;
import com.ofbusiness.assignment.dto.ChatlogDto;
import com.ofbusiness.assignment.repository.ChatlogRepository;
import com.ofbusiness.assignment.service.ChatlogService;

@Service
public class ChatlogServiceImpl implements ChatlogService {

	@Autowired
	private ChatlogRepository chatLogRepository;

	@Override
	public long createChatLogEntry(ChatlogDto chatLogDto, String userId) throws Exception {

		long messageId = 0;

		if (userId.length() <= 0 || userId.trim().isEmpty() || userId == null) {
			throw new IllegalArgumentException("Invalid param UserId - [" + userId + "]");
		}

		if (userId.length() >= 16) {
			throw new IllegalArgumentException("Invalid param UserId - [" + userId + "] length should be less than 16");
		}

		if (chatLogDto.getIsSent() == 0 || chatLogDto.getIsSent() == 1) {
			Chatlog chatLog = new Chatlog();
			chatLog.setUserId(userId);
			chatLog.setMessageText(chatLogDto.getMessageText());
			chatLog.setIsSent(chatLogDto.getIsSent());
			chatLog.setTimestamp(new Date());
			chatLog = chatLogRepository.save(chatLog);

			messageId = chatLog.getChatLogId();
			return messageId;
		}

		else {
			throw new IllegalArgumentException(
					"Invalid param - isSent  [" + chatLogDto.getIsSent() + "] It should either be 1 or 0");
		}

	}

	@Override
	public int removeUserChatLogByMsgId(String userId, int chatId) throws Exception {

		if (userId == null || userId.length() <= 0 || userId.trim().isEmpty()) {
			throw new IllegalArgumentException("Invalid param UserId - [" + userId + "]");
		}

		if (userId.length() >= 16) {
			throw new IllegalArgumentException("Invalid param UserId - [" + userId + "] length should be less than 16");
		}

		if (chatId <= 0) {
			throw new IllegalArgumentException("Invalid param chatId - [" + chatId + "]");
		}

		if (checkUserIdExist(userId)) {

			if (checkChatIdExist(chatId)) {
				return chatLogRepository.deleteUserChatByMsgId(userId, chatId);
			} else {
				throw new IllegalArgumentException("Invalid Chat Id - does not exist");
			}
		} else {
			throw new IllegalArgumentException("Invalid User Id - does not exist");
		}

	}

	@Override
	public boolean checkUserIdExist(String userId) throws Exception {
		int count = 0;
		count = chatLogRepository.checkUserExist(userId);
		return count > 0 ? true : false;
	}

	@Override
	public boolean checkChatIdExist(int chatId) throws Exception {
		int count = 0;
		count = chatLogRepository.checkChatIdExist(chatId);
		return count > 0 ? true : false;
	}

	@Override
	public int removeUserChatLog(String userId) throws Exception {

		if (userId == null || userId.length() <= 0 || userId.trim().isEmpty()) {
			throw new IllegalArgumentException("Invalid param UserId - [" + userId + "]");
		}

		if (userId.length() >= 16) {
			throw new IllegalArgumentException("Invalid param UserId - [" + userId + "] length should be less than 16");
		}

		if (checkUserIdExist(userId)) {

			return chatLogRepository.deleteUserChatLog(userId);

		} else {
			throw new IllegalArgumentException("Invalid User Id - does not exist");
		}

	}

	@Override
	public List<Chatlog> retrieveChatLogByUserId(String userId, int limit, int page) throws Exception {

		if (userId == null || userId.length() <= 0 || userId.trim().isEmpty()) {
			throw new IllegalArgumentException("Invalid param UserId - [" + userId + "]");
		}

		if (userId.length() >= 16) {
			throw new IllegalArgumentException("Invalid param UserId - [" + userId + "] length should be less than 16");
		}

		if (!checkUserIdExist(userId)) {
			throw new IllegalArgumentException("Invalid User Id - does not exist");
		}

		else {

			Pageable pageElements = PageRequest.of(page, limit, Sort.by(Sort.Direction.DESC, "timestamp"));

			List<Chatlog> result = chatLogRepository.findAll(new Specification<Chatlog>() {
				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;

				public Predicate toPredicate(Root<Chatlog> root, CriteriaQuery<?> query,
						CriteriaBuilder criteriaBuilder) {

					List<Predicate> predicates = new ArrayList<>();

					predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("isDeleted"), false)));

					return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
				}
			}, pageElements);

			if (result.size() <= 0) {
				throw new IllegalArgumentException("No Chat logs found for the userId [" + userId + "]");
			}

			return result;
		}
	}
}