package com.ofbusiness.assignment.service;

import java.util.List;

import com.ofbusiness.assignment.domain.Chatlog;
import com.ofbusiness.assignment.dto.ChatlogDto;

public interface ChatlogService {

	public long createChatLogEntry(ChatlogDto chatLogDto, String userId) throws Exception;

	public List<Chatlog> retrieveChatLogByUserId(String userId, int maxCount, int page) throws Exception;

	public int removeUserChatLogByMsgId(String userId, int chatId) throws Exception;

	public int removeUserChatLog(String userId) throws Exception;

	public boolean checkUserIdExist(String userId) throws Exception;

	public boolean checkChatIdExist(int chatId) throws Exception;

}
