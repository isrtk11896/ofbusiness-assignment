package com.ofbusiness.assignment.response;

public class APIResponse {

	private Object response;

	private String message;

	private boolean status;

	public APIResponse(Object response, String message, boolean status) {
		super();
		this.response = response;
		this.message = message;
		this.status = status;
	}

	public Object getResponse() {
		return response;
	}

	public void setResponse(Object response) {
		this.response = response;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

}
